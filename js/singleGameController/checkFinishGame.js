function checkGameFinish(){
  // Check rows
  if( squares[0].player === currentPlayer.player && 
      squares[1].player === currentPlayer.player && 
      squares[2].player === currentPlayer.player){
        message = 'Gana jugador: ' + currentPlayer.player;
        modalNode(message);
        blockBoard();
  }
  if( squares[3].player === currentPlayer.player && 
      squares[4].player === currentPlayer.player && 
      squares[5].player === currentPlayer.player){
        message = 'Gana jugador: ' + currentPlayer.player;
        modalNode(message);
        blockBoard();
  }
  if( squares[6].player === currentPlayer.player && 
      squares[7].player === currentPlayer.player && 
      squares[8].player === currentPlayer.player){
        message = 'Gana jugador: ' + currentPlayer.player;
        modalNode(message);
        blockBoard();
  }

  // Check columns
  if( squares[0].player === currentPlayer.player && 
      squares[3].player === currentPlayer.player && 
      squares[6].player === currentPlayer.player){
        message = 'Gana jugador: ' + currentPlayer.player;
        modalNode(message);
        blockBoard();
  }
  if( squares[1].player === currentPlayer.player && 
      squares[4].player === currentPlayer.player && 
      squares[7].player === currentPlayer.player){
        message = 'Gana jugador: ' + currentPlayer.player;
        modalNode(message);
        blockBoard();
  }
  if( squares[6].player === currentPlayer.player && 
      squares[7].player === currentPlayer.player && 
      squares[8].player === currentPlayer.player){
        message = 'Gana jugador: ' + currentPlayer.player;
        modalNode(message);
        blockBoard();
  }
      
  //Check diagonal
  if( squares[0].player === currentPlayer.player && 
      squares[4].player === currentPlayer.player && 
      squares[8].player === currentPlayer.player){
        message = 'Gana jugador: ' + currentPlayer.player;
        modalNode(message);
        blockBoard();
  }
  if( squares[6].player === currentPlayer.player && 
      squares[4].player === currentPlayer.player && 
      squares[2].player === currentPlayer.player){
        message = 'Gana jugador: ' + currentPlayer.player;
        modalNode(message);
        blockBoard();
  }
  if(gamePlaying === true){
    // Check if all squares are busy
    countSquaresBusies = 0;
    for(i=0; i<=8; i++){
      if(squares[i].busy === true){
        countSquaresBusies = countSquaresBusies + 1;
      }
    }
    if(countSquaresBusies === 9){
      message = 'Estan ocupadas todas las casillas, empieza otra partida';
      modalNode(message);
      blockBoard();
    }
  }
}