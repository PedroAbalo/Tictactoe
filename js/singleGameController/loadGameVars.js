//create board var
var viewport = document.getElementById('viewportGame'); 

//create board var
var board = document.getElementById('board_game'); 

//Create squares var for all square object
var squares = [];

//Create game state
var gamePlaying = false;

//Define type of pieces

var playerX = {
  node: '<div class="pieceScale">X</div>',
  player: 'X'
};
var playerO = {
  node: '<div class="pieceScale">O</div>',
  player: 'O'
};

//Create current player, X always start game
var currentPlayer = playerX;

//Define squares objects
for(i=0; i<=8; i++){
  squares[i] = {};
  squares[i].node = document.getElementById('c'+i); //Add node prop to square
}