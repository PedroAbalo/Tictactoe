function initGame(){
  // Turn game on playing state
  gamePlaying = true;

  // Throught squares
  squares.forEach( function(square){
      
      squareObj = square;
      squareObj.node.innerHTML = ""; //Delete pieces in square
      squareObj.node.addEventListener("click", function(){
          squareObj = this;

          //Get the id of node
          squareObjId = squareObj.id;
          squareObjId = squareObjId.substring(1);

          //Set object parent to var
          squareObj = squares[squareObjId];
          checkSquare(squareObj);
      }); //Turn on click listening
  });
} 
initGame();